require("ext.evt.evtInit")
require("ext.ui.uiInit")

local UITestScene = class("UITestScene",BaseScene)

function UITestScene:ctor()
    UITestScene.super.ctor(self)
end

function UITestScene:onEnter()
--    local uiReader = ccs.GUIReader:getInstance()
--    local layer = cc.Layer:create()
--    self:addChild(layer)
--    local ui = uiReader:widgetFromJsonFile("ui/DemoHead_UI/DemoHead_UI.json")
--    layer:addChild(ui)

    --UI 根容器
    local root = Component.new()
    root:setPosition(0,display.height)
    self:addChild(root)
    
    local layer1 = Component.new()
    layer1:setUIPosition(display.cx,display.cy)
    root:addChild(layer1)
    local tt = Component.new()

    self:addUIImage(layer1)
    self:addProgressBar(layer1)
    self:createButton(layer1)
    self:createTabBar(root)
    self:createScale3Image(root)
    self:createScale9Image(root)
    self:createTextField(root)
    self:createCharFont(root)
--    self:createUIScrollView(root)
--    self:createUIPageScrollView(root)
    self:createCenterScrollView(root)
    self:createSlider(root)
    self:createInputText(root)
    self:createMovieClip(root)
    self:createTweener(layer1)
    
    --测试alpha
    layer1:setAlpha(0.8)
    root:setAlpha(0.8)
    
    local function timeout()
        root:setAlpha(1)
    end
    self:setTimeout(2,timeout)
end



function UITestScene:createTweener(tweenObj)
    
    local o = {x=0,y=0}

    local function onStart(target,param)
        print("onStart")
    end
    local function onUpdate()
        tweenObj:setUIPosition(o.x,o.y)
    end 

    Tweener.addTween(o, {
        x=display.width, y=display.height,
        yoyo = true, target = self,
        time=1, delay=1, transition=Tweener.EaseInOutBounce,
        onStart = onStart,
        onStartParams = {start=1},
        onUpdate = onUpdate,
        onComplete=self.onTweenComplete,
        onCompleteParams={o=o}
    })
end

function UITestScene:onTweenComplete(params)
--    Tweener.kill(params.o)
end






function UITestScene:addProgressBar(container)
    local proTimer = cc.ProgressTimer:create( cc.Sprite:create("ui/proBarfore.png"))
    proTimer:setType(cc.PROGRESS_TIMER_TYPE_BAR)
    proTimer:setReverseDirection(true)
    proTimer:setBarChangeRate(cc.p(1,0))
    proTimer:setMidpoint(cc.p(1,0))

    self.progress = ProgressBar.new( proTimer, "ui/proBarBg.png" )
    self.progress:setUIPosition(0,100)
    self.progress:setPercentage(40)

    container:addChild( self.progress)
end



function UITestScene:addUIImage(container)
    local img = Image.new("dog.png")
    img.touchEnable = true
    img:setUIPosition(200,-130)
    img:setRect(cc.rect(0,0,150,50))
    img.dispatcher:addEventListener(TouchEvent.CLICK , self.onImageClick , self )
    img:setScale(0.4,0.75)
    img:showBorder()
    container:addChild(img)
end
function UITestScene:onImageClick(event)
    print("Image click")
end



function UITestScene:createButton(container)
    local btn = Button.new("ui/btn_normal.png")
    btn.enableTouchScale = true
    btn:setUIPosition(0,-200)
    container:addChild(btn)
    btn.dispatcher:addEventListener(TouchEvent.CLICK,self.onClickButton,self)
end
function UITestScene:onClickButton(evt)
    print("onClick Button")
end





function UITestScene:createTabBar( container )
	
    local btns={}
    for i=1, 4 do
        local btn = ToggleButton.new(
            cc.Sprite:create("ui/checkbox_normal.png"),
            cc.Sprite:create("ui/checkbox_selected.png"),
            cc.Sprite:create("ui/checkbox_normal_disabled.png"),
            cc.Sprite:create("ui/checkbox_selected_disabled.png")
        )
        btn.enableTouchScale = true
        btn:setUIPosition(100*(i-1),0)
        btns[#btns+1] = btn
    end
    local tab = TabBar.new(btns)
    tab.dispatcher:addEventListener(Event.CHANGE ,self.onChange,self)
    tab:setSelectedButton(btns[2])
    tab:setUIPosition(50,50)
    container:addChild(tab)
    
end
function UITestScene:onChange(evt)
	print("TabBar onChange")
end




function UITestScene:createScale3Image( container )
    local img = Scale3Image.new("ui/checkbox_normal.png",40,20,Scale3Image.DIRECTION_HORIZONTAL)
    img:setWidth(display.width/2)
    img.touchEnable = true
    img:setUIPosition(display.cx+350,display.cy+200)
    container:addChild(img)
    img.dispatcher:addEventListener(TouchEvent.CLICK , self.onScale3ImageClick,self)
end
function UITestScene:onScale3ImageClick(event)
    print("click Scale3Image")
end




function UITestScene:createScale9Image( container )
    local img = Scale9Image.new("ui/checkbox_normal.png",40,60,40,60)
    img.touchEnable = true
    img:setUIPosition(display.cx,display.cy)
    container:addChild(img)
    img.dispatcher:addEventListener(TouchEvent.CLICK,self.onScale9ImageClick,self)
    
    img:setSize(300,200)
end
function UITestScene:onScale9ImageClick(event)
    print("click Scale9Image")
end



function UITestScene:createTextField(container)
    local font = "fonts/HKYuanMini.ttf" --"fonts/font-issue1343.fnt" 
    local tf = TextField.new("begin hello world ,你好, hello world, hello world end",font)
    tf:setHAlign(cc.TEXT_ALIGNMENT_RIGHT)
    tf:setVAlign(cc.VERTICAL_TEXT_ALIGNMENT_TOP)
    tf:setTextWidth(400)
--    tf:setTextHeight(100)
    tf:setColor(cc.c3b(255,255,0))
    tf:setContentSize(560,100)
    tf.touchEnable = true
    tf:setUIPosition(display.width/2,display.height/2)
--    tf:setAnchorPoint(1,1)
    tf:showBorder()
    container:addChild(tf)
    tf.dispatcher:addEventListener(TouchEvent.CLICK,self.onTFClick , self)
end
function UITestScene:onTFClick(evt)
    print("TextField Click")
end



function UITestScene:createCharFont(container)
	local font = CharFont.new("123459.123","fonts/labelatlas.png",16,32,".")
	font.touchEnable = true
    font:setUIPosition(250,200)
    font:setContentSize(100,50) --自动缩放
	font:showBorder()
	container:addChild(font)
	font.dispatcher:addEventListener(TouchEvent.CLICK,self.onCharFontClick,self)
end
function UITestScene:onCharFontClick(evt)
	print("click Char Font")
end



function UITestScene:createSlider(container)
    local bg = Scale3Image.new("ui/checkbox_normal.png",40,20,Scale3Image.DIRECTION_VERTICAL)
    bg:setHeight(display.height/2)
    bg.touchEnable = true
    local thumb = Image.new("ui/checkbox_normal.png")
    thumb:setScaleX(1.2)
    thumb:setColor(cc.c3b(255,126,0))
    thumb.touchEnable = true
    local slider=  Slider.new(Slider.DIRECTION_VERTICAL,bg,thumb)
    slider:setUIPosition(200,display.cy)
    container:addChild(slider)
    slider.dispatcher:addEventListener(Event.CHANGE,self.onSliderChange,self)
end
function UITestScene:onSliderChange(evt)
	print("slider change:"..evt.userdata)
end



function UITestScene:createUIScrollView(container)
    local scroll = UIScrollView.new(cc.size(display.width-100,display.height-100),UIScrollView.DIRECTION_VERTICAL,3) --DIRECTION_HORIZONTAL,DIRECTION_VERTICAL
    local renders={}
    local font = "fonts/HKYuanMini.ttf" --"fonts/font-issue1343.fnt" 
    for var=1, 38 do
        local img = cc.Sprite:create("ui/btn_normal.png")
        img:setColor(cc.c3b(255,255,0))
        local sp = Button.new(img)
        sp.enableTouchScale = true
--        local tf = TextField.new(var,font)
--        sp:addChild(tf)
--        tf:getLabel():enableOutline(cc.c4b(255,0,0,255),2)
    	sp.dispatcher:addEventListener(TouchEvent.REAL_CLICK,self.onClickScrollViewItem,self)
    	table.insert(renders,sp)
    end
    scroll:setRenders(renders)
    scroll:setUIPosition(display.cx,display.cy)
    container:addChild(scroll)
    scroll:showBorder()
end

function UITestScene:onClickScrollViewItem( evt )
    print("click scrollView item")
end




function UITestScene:createUIPageScrollView(container)
    self.pageScroll = UIPageScrollView.new(cc.size(display.width-250,200),UIPageScrollView.DIRECTION_HORIZONTAL,3) --DIRECTION_HORIZONTAL,DIRECTION_VERTICAL
    local font = "fonts/HKYuanMini.ttf" --"fonts/font-issue1343.fnt" 
    for var=1, 38 do
        local img = Image.new("ui/btn_normal.png")
        img:setColor(cc.c3b(255,255,0))
        
        local tf = TextField.new(var,font)
        tf:getLabel():enableOutline(cc.c4b(255,0,0,255),3)
        img:addChild(tf)
        
        img.dispatcher:addEventListener(TouchEvent.REAL_CLICK,self.onClickPageScrollViewItem,self)
        self.pageScroll:addItem(img)
    end
    self.pageScroll:setUIPosition(display.cx,display.height-100)
    container:addChild(self.pageScroll)
    self.pageScroll:showBorder()
    self.pageScroll.dispatcher:addEventListener(ScrollEvent.SCROLL_OVER,self.onPageScrollHandler,self)
    self.pageScroll.dispatcher:addEventListener(ScrollEvent.SCROLL_POSITION_CHANGE,self.onPageScrollHandler,self)
    self.pageScroll.dispatcher:addEventListener(ScrollEvent.TOUCH_MOVED,self.onPageScrollHandler,self)
    
    local prevBtn = Button.new("ui/btn_normal.png")
    prevBtn:setScale(0.5,0.5)
    prevBtn:setUIPosition(50,display.height-100)
    container:addChild(prevBtn)
    prevBtn.enableTouchScale = true 
    prevBtn.dispatcher:addEventListener( TouchEvent.CLICK,self.onPrevPageHandler,self)
    
    local nextBtn = Button.new("ui/btn_normal.png")
    nextBtn:setScale(0.5,0.5)
    nextBtn.enableTouchScale = true 
    nextBtn:setUIPosition(display.width-50,display.height-100)
    container:addChild(nextBtn)
    nextBtn.dispatcher:addEventListener( TouchEvent.CLICK,self.onNextPageHandler,self)
    
    local text="page:"..self.pageScroll:getCurrentPage().."/"..self.pageScroll:getTotalPage()
    self.pageTxt = TextField.new(text,font)
    self.pageTxt:setUIPosition(display.cx,display.height-20)
    container:addChild(self.pageTxt)
end
function UITestScene:onClickPageScrollViewItem(evt)
    print("onClickPageScrollViewItem")
end
function UITestScene:onPrevPageHandler(evt)
	self.pageScroll:prevPage()
end
function UITestScene:onNextPageHandler(evt)
	self.pageScroll:nextPage()
end
function UITestScene:onPageScrollHandler(evt)
	print(evt.type)
    if evt.type==ScrollEvent.SCROLL_OVER then
	   local text="page:"..self.pageScroll:getCurrentPage().."/"..self.pageScroll:getTotalPage()
        self.pageTxt:setText(text)
	end
end






function UITestScene:createCenterScrollView(container)
    self.centerScroll = UICenterScrollView.new(cc.size(display.width-250,200),UIPageScrollView.DIRECTION_HORIZONTAL) --DIRECTION_HORIZONTAL,DIRECTION_VERTICAL
    local font = "fonts/HKYuanMini.ttf" --"fonts/font-issue1343.fnt" 
    for var=1, 15 do
        local img = Image.new("ui/btn_normal.png")
        img:setColor(cc.c3b(255,255,0))

        local tf = TextField.new(var,font)
        tf:getLabel():enableOutline(cc.c4b(255,0,0,255),3)
        img:addChild(tf)

        self.centerScroll:addItem(img)
    end
    self.centerScroll:setUIPosition(display.cx,display.height-100)
    container:addChild(self.centerScroll)
    self.centerScroll:showIndex(2,true)
    self.centerScroll:showBorder()
    self.centerScroll.dispatcher:addEventListener(ScrollEvent.SCROLL_OVER,self.onCenterScrollHandler,self)
    self.centerScroll.dispatcher:addEventListener(ScrollEvent.SCROLL_POSITION_CHANGE,self.onCenterScrollHandler,self)
    self.centerScroll.dispatcher:addEventListener(ScrollEvent.TOUCH_MOVED,self.onCenterScrollHandler,self)

    local prevBtn = Button.new("ui/btn_normal.png")
    prevBtn:setScale(0.5,0.5)
    prevBtn.enableTouchScale = true
    prevBtn:setUIPosition(50,display.height-100)
    container:addChild(prevBtn)
    prevBtn.dispatcher:addEventListener( TouchEvent.CLICK,self.onPrevCenterHandler,self)

    local nextBtn = Button.new("ui/btn_normal.png")
    nextBtn:setScale(0.5,0.5)
    nextBtn.enableTouchScale = true
    nextBtn:setUIPosition(display.width-50,display.height-100)
    container:addChild(nextBtn)
    nextBtn.dispatcher:addEventListener( TouchEvent.CLICK,self.onNextCenterHandler,self)

    local text="page:"..self.centerScroll:getCurrentPage().."/"..self.centerScroll:getTotalPage()
    self.pageTxt = TextField.new(text,font)
    self.pageTxt:setUIPosition(display.cx,display.height-20)
    container:addChild(self.pageTxt)
end
function UITestScene:onPrevCenterHandler(evt)
    self.centerScroll:prevPage()
end
function UITestScene:onNextCenterHandler(evt)
    self.centerScroll:nextPage()
end
function UITestScene:onCenterScrollHandler(evt)
    print(evt.type)
    if evt.type==ScrollEvent.SCROLL_OVER then
        local text="page:"..self.centerScroll:getCurrentPage().."/"..self.centerScroll:getTotalPage()
        self.pageTxt:setText(text)
    end
end






function UITestScene:createInputText(container)
	local input = TextInput.new(cc.size(300,50),"ui/checkbox_selected_disabled.png")
	input:setPlaceHolder("test:")
    input:setUIPosition(display.width-300,50)
    input:setMaxLength(8)
	container:addChild(input)
end



function UITestScene:createMovieClip(container)
    local mc = MovieClip.new()
    container:addChild(mc)
	
	local leftRunAnimation = MovieClip.createAnimationWithRect(self,"ui/character.png",cc.p(0,0),408/4,276/2,4,0.08,true)
    local leftRunAction = cc.RepeatForever:create( cc.Animate:create(leftRunAnimation))
    mc:addAnimationAction("leftRun",leftRunAction)

    local rightRunAnimation = MovieClip.createAnimationWithRect(self,"ui/character.png",cc.p(0,276/2),408/4,276/2,4,0.08,true)
    local rightRunAction = cc.RepeatForever:create( cc.Animate:create(rightRunAnimation))
    mc:addAnimationAction("rightRun",rightRunAction)
    
    mc:playAnimation("leftRun")
    mc:setUIPosition(display.width-200,100)
    
    mc.touchEnable = true
    mc.dispatcher:addEventListener( TouchEvent.CLICK, self.onClickMoviclip , self)
end

function UITestScene:onClickMoviclip(evt)
    if evt.target:getCurrentAnimationName()=="leftRun" then
        evt.target:playAnimation("rightRun")
    else
        evt.target:playAnimation("leftRun")
    end
end


return UITestScene