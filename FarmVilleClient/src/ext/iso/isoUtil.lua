Y_CORRECT = math.cos(-math.pi / 6) * math.sqrt(2)
W_H_RATE = 0.75 --宽高比,0.75为coc地图，格式宽为40，高为30。0.5为正常的45度地图，格式宽为60，高为30

local isoUtil  = isoUtil or {}

function isoUtil.isoToScreen(px,py,pz)
	local screenX = px - pz 
	local screenY = py * Y_CORRECT + (px+pz) * W_H_RATE
	return screenX, -screenY
end

function isoUtil.screenToIso(px,py)
	local zpos = ( -py-px*W_H_RATE )/(2*W_H_RATE)
	local xpos = px + zpos
	local ypos = 0 

	return xpos,ypos,zpos
end


function isoUtil.screenToIsoGrid( size , px , py ) 
	-- local xpos = -py + px * 0.5
	-- local zpos = -py - px * 0.5
	local zpos = ( -py-px*W_H_RATE )/(2*W_H_RATE)
	local xpos = px + zpos
	
	local col = math.ceil ( xpos / size ) 
	local row = math.ceil ( zpos / size ) 
	return col,row 
end


return isoUtil