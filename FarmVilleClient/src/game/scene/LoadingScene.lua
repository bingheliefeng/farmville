local LoadingScene = class("LoadingScene",BaseScene)

function LoadingScene:ctor()
    LoadingScene.super.ctor(self) --调用父类构造
   
   
end

function LoadingScene:onEnter()
    
    local ttfConfig = {}
    ttfConfig.fontFilePath="res/fonts/Marker Felt.ttf"
    ttfConfig.fontSize=40
    self.label = cc.Label:createWithTTF(ttfConfig,"Loading...", cc.VERTICAL_TEXT_ALIGNMENT_CENTER,256)
    self:addChild(self.label)
    self.label:setPosition(display.cx,display.height-50)
    

    local shareTextureCache = cc.Director:getInstance():getTextureCache()
    local loadedCount = 0
    local function onLoaded()
        loadedCount = loadedCount+1
        self.label:setString("Loading..."..(loadedCount/5*100).."%")
        if loadedCount==5 then
            self:setTimeout(1,handler(self,self.onLoadComplete))
        end
    end
    shareTextureCache:addImageAsync("res/isoRes/Background.png",onLoaded)
    shareTextureCache:addImageAsync("res/isoRes/forest.png",onLoaded)
    shareTextureCache:addImageAsync("res/isoRes/grass.png",onLoaded)
    shareTextureCache:addImageAsync("res/isoRes/host.png",onLoaded)
    shareTextureCache:addImageAsync("res/isoRes/stove.png",onLoaded)
end

function LoadingScene:onLoadComplete()
    if DEBUG>0 then
        print("初始化资源加载完成")
    end
    App.enterGameScene()
end

return LoadingScene
