local isoUtil = require("ext.iso.isoUtil")

local IsoObject = class("IsoObject",function()
	return cc.Node:create()
end)


function IsoObject:ctor( size , spanX, spanZ)
	spanX = spanX or 1
	spanZ = spanZ or 1

	self._size = size 
	self._spanX = spanX
	self._spanZ = spanZ

	self.isSort = false 

	self._pos3D = {x=0,y=0,z=0}
	self._nodeX = 1
	self._nodeZ = 1
	self._spanPosArray = {}
	self._isRotate = false
	self._boundingBox = cc.rect(0,0,0,0)
	self._centerYConst = 0;
end

function IsoObject:getSpanX() 
	return self._spanX 
end
function IsoObject:getSpanZ() 
	return self._spanZ 
end
function IsoObject:getCenterY() 
	return self:getPositionY()+self._centerYConst 
end
function IsoObject:getSize()
	return self._size
end
function IsoObject:boundingBox()
	self._boundingBox.origin.x = self:getX()
	self._boundingBox.origin.y = self:getZ()
	if self._isRotate then
		self._boundingBox.size.width = self._size*(self._spanZ-1) 
		self._boundingBox.size.height = self._size*(self._spanX-1) 
	else
		self._boundingBox.size.height = self._size*(self._spanZ-1) 
		self._boundingBox.size.width = self._size*(self._spanX-1) 
	end
	return self._boundingBox
end


function IsoObject:sort()
	self.isSort = true 
end

function IsoObject:rotateX( value )
	self._isRotate = value
	self:updateSpanPos() 
end

function IsoObject:updateScreenPos()
	local x,y = isoUtil.isoToScreen(self._pos3D.x,self._pos3D.y,self._pos3D.z)
	self:setPositionX(x)
	self:setPositionY(y+self._size)
	self:updateSpanPos()
end

function IsoObject:updateSpanPos()
	self._spanPosArray = {}
	local t1,t2
	if self._isRotate then
		t1 = self._spanZ
		t2 = self._spanX
	else
		t1 = self._spanX
		t2 = self._spanZ
	end

	for i=1,self._spanZ do
		for j=1,self._spanX do
			local pos = {
				x = (i-1)*self._size+self:getX() , 
				y = self:getY() , 
				z = (j-1)*self._size+self:getZ() 
			}
			self._spanPosArray[ #self._spanPosArray+1 ] = pos
		end
	end
end
function IsoObject:getSpanPosArray()
	return self._spanPosArray
end


function IsoObject:setX( value )
	self._pos3D.x = value
	self:updateScreenPos()
	self:updateSpanPos()
end
function IsoObject:getX()
	return self._pos3D.x
end

function IsoObject:setY( value )
	self._pos3D.y = value
end
function IsoObject:getY()
	return self._pos3D.y 
end
function IsoObject:setZ( value )
	self._pos3D.z = value 
	self:updateScreenPos()
	self:updateSpanPos()
end
function IsoObject:getZ()
	return self._pos3D.z
end
function IsoObject:setPos3D( value )
	self._pos3D = value
	self:updateScreenPos()
	self:updateSpanPos()
end
function IsoObject:getPos3D()
	return self._pos3D
end
function IsoObject:getDepth()
	return (self._pos3D.x + self._pos3D.z) * .866 - self._pos3D.y * .707;
end


function IsoObject:setNodeX(value)
	self._nodeX = value ;
	self:setX( value * self._size)
end
function IsoObject:getNodeX()
	return self:getX()/self._size
end
function IsoObject:setNodeZ(value)
	self._nodeZ = value ;
	self:setZ( value * self._size)
end
function IsoObject:getNodeZ()
	return self:getZ()/self._size
end


function IsoObject:setScreenPos(x,y)
	self:setX(x)
	self:setY(y)
	self._pos3D.x = x 
	self._pos3D.y = y
end

function IsoObject:setWalkable( value,pathGird )
	self:updateSpanPos()
	for _,v in ipairs(self._spanPosArray) do
		pathGird:setWalkable( math.floor(v.x/self._size) , math.floor(v.z/self._size) , value )
	end
end

function IsoObject:getWalkable(pathGrid)
	local flag = false
	for _,v in ipairs(self._spanPosArray) do
		local nodeX = math.floor(v.x/self._size)
		local nodeY = math.floor(v.z/self._size)
		if  nodeX<1 or nodeX>pathGrid:getGridX() then return false end
		if  nodeY<1 or nodeY>pathGrid:getGridZ() then return false end
		flag = pathGrid:getNode( nodeX , nodeY ).walkable 
		if  not flag then return false end
	end
	return true
end


function IsoObject:getRotatable(pathGrid)
	if self._spanX==self.spanZ then return true end
			
	self:setWalkable(true,pathGrid)
	self._isRotate = not self._isRotate
	self:updateSpanPos()
	local flag = self:getWalkable(pathGrid)
	--还原
	self._isRotate = not self._isRotate
	self:setWalkable(false,pathGrid)
	return bool
end


function IsoObject:toString()
	return "[IsoObject (x:" .. _position3D.x .. ", y:" .. _position3D.y .. ", z:" .. _position3D.z .. ") ]";
end

function IsoObject:dispose()
	self._boundingBox = nil 
	self._spanPosArray = nil 
	self._pos3D = nil 
end

return IsoObject