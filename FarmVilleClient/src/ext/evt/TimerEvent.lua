--
-- Author: zhouzhanglin
-- Date: 2014-07-14 17:09:13
--
local TimerEvent = class("TimerEvent",require("ext.evt.Event"))

TimerEvent.TIMER = "timer"
TimerEvent.TIMER_COMPLETE = "timerComplete"

return TimerEvent