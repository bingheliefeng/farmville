GRID_SIZE = 40 --size大小
GIRD_X = 32  --x轴的网格大小
GIRD_Z = 25  -- z轴的网格大小
MAP_WIDTH = 1024*2
MAP_HEIGHT = 768*2

--iso游戏，继承自BaseLayer
local BaseWorld = class("BaseWorld",IsoWorld)

function BaseWorld:ctor()
    BaseWorld.super.ctor(self, GRID_SIZE , GIRD_X , GIRD_Z)

    self._moveSpeed = 0 --地图移动速度，在点击began中设置
    self._touches = {}
    self._scaleMatrix = Matrix.new()

    self._gridData = PathGrid.new(GIRD_X,GIRD_Z)
    self._gridData:setAllWalkable(true)


    self:setPosition(display.left-MAP_WIDTH/2+display.cx,display.top+100)
    self:panSceneLayerTo(MAP_WIDTH/2,-360 )
    --添加背景图片
    local bg = cc.Sprite:create("res/isoRes/Background.png")
    bg:setAnchorPoint(cc.p(0,1))
    bg:setScale(2,2)
    self:setBg(bg)


    self:addIosScenes()
    self:addWorldListener()
end

function BaseWorld:addIosScenes()

    --添加isoScene
    --添加网格
    local gridNode = IsoGrid.new(GRID_SIZE,GIRD_X,GIRD_Z,0.4)
    self:addScene(gridNode)
    self.buildingLayer = require("game.core.layers.BuildingLayer").new(GRID_SIZE,GIRD_X,GIRD_Z)
    self:addScene(self.buildingLayer)

    --添加iso对象
    for i=1,GIRD_X ,2 do
        for j=1,GIRD_Z,2 do
            if math.random()<0.5 then
                local node = W_H_RATE==0.5 and self:createNode() or self:createCocNode()
                node:setNodeX(i)
                node:setNodeZ(j)
                if node:getWalkable(self._gridData) then
                    node:setWalkable(false,self._gridData)
                    self.buildingLayer:addIsoObject(node, false )
                else
                    node = nil
                end
            end
        end
    end
    self.buildingLayer:sortAll()

    self._endX = self:getPositionX()
    self._endY = self:getPositionY()



    local ttfConfig = {}
    ttfConfig.fontFilePath="res/fonts/Marker Felt.ttf"
    ttfConfig.fontSize=40
    local label = cc.Label:createWithTTF(ttfConfig,"COC MAP", cc.VERTICAL_TEXT_ALIGNMENT_CENTER,256)
    self:addChild(label)
    label:setPosition(display.cx,display.height-50)

end



function BaseWorld:createCocNode()
    local node = IsoObject.new(GRID_SIZE,2,2)
    local sp = cc.Sprite:create("res/isoRes/host.png")
    sp:setPositionY(-38)

    local listener = cc.EventListenerTouchOneByOne:create()
    listener:registerScriptHandler(function(touch,event) 
        sp._moved = false
        --判断是否在点击区域内，如果不在返回false
        if display.checkTouch(touch,event) then

            return true
        end
    end,cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(function(touch,event)
        local point = touch:getLocation()
        local prevPoint = touch:getPreviousLocation()
        if cc.pGetDistance( cc.p(point.x,point.y),cc.p(prevPoint.x,prevPoint.y))>5 then
            sp._moved = true
        end
    end,cc.Handler.EVENT_TOUCH_MOVED)

    local function onTouchOver(touch,event) 
        if not sp._moved then
            event:stopPropagation()
            self:onClick( node )
        end
    end
    listener:registerScriptHandler(onTouchOver ,cc.Handler.EVENT_TOUCH_ENDED)
    listener:registerScriptHandler(onTouchOver ,cc.Handler.EVENT_TOUCH_CANCELLED)
    sp:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener,sp)
    node:addChild(sp)

    return node
end




--[[
添加world 侦听.
--]]
function BaseWorld:addWorldListener()
    local function onTouchesBegan(touches, event)
        for _, touch in pairs(touches) do
            local id = touch:getId()
            local point = touch:getLocation()
            self._touches[#self._touches+1] = { 
                beganX=point.x, beganY=point.y,
                currentX=point.x, currentY=point.y,
                prevX=point.x, prevY=point.y,
                type="began" , id = id
            }
        end
        self:onWorldTouchBegan()
        return true
    end
    local function onTouchesMoved(touches, event)
        for _, touch in pairs(touches) do
            local id = touch:getId()
            local point = touch:getLocation()
            local prevPoint = touch:getPreviousLocation()
            for _,v in ipairs(self._touches) do
                if v.id == id then
                    v.currentX = point.x 
                    v.currentY = point.y 
                    v.prevX = prevPoint.x
                    v.prevY = prevPoint.y 
                    v.type = "moved"
                end
            end
        end

        self:onWorldTouchMoved()
    end
    local function onTouchOver(touches,event, type)
        if #self._touches~=0 then
            for _, touch in ipairs(touches) do
                local id = touch:getId()
                local point = touch:getLocation()
                for _,v in ipairs(self._touches) do
                    if v.id == id then
                        v.currentX = point.x 
                        v.currentY = point.y 
                        v.type = type
                    end
                end
            end
        end

        self:onWorldTouchEnded()
        self._touches={}
    end 
    local listener = cc.EventListenerTouchAllAtOnce:create()
    listener:registerScriptHandler(onTouchesBegan,cc.Handler.EVENT_TOUCHES_BEGAN )
    listener:registerScriptHandler(onTouchesMoved,cc.Handler.EVENT_TOUCHES_MOVED  )
    listener:registerScriptHandler(onTouchOver,cc.Handler.EVENT_TOUCHES_ENDED  )
    listener:registerScriptHandler(onTouchOver,cc.Handler.EVENT_TOUCHES_CANCELLED )
    self:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener, self)

end



function BaseWorld:update(dt)
    self:setPosition( 
        self:getPositionX()+(self._endX-self:getPositionX())*self._moveSpeed,
        self:getPositionY()+(self._endY-self:getPositionY())*self._moveSpeed
    )
end


function BaseWorld:onWorldTouchBegan(points)
    if #self._touches == 1 then
        self._moveSpeed = 0.4
        self._beganWorldX = self:getPositionX()
        self._beganWorldY = self:getPositionY()
    end
end

function BaseWorld:onWorldTouchMoved()
    if #self._touches == 1 then
        local deltaX,deltaY= self._touches[1].currentX-self._touches[1].beganX , self._touches[1].currentY-self._touches[1].beganY
        self._endX = self._beganWorldX+deltaX 
        self._endY = self._beganWorldY+deltaY
        self:modifyEndPosition()

    elseif #self._touches>=2 then
        --中点
        local middleX = (self._touches[1].beganX+self._touches[2].beganX)/2
        local middleY = (self._touches[1].beganY+self._touches[2].beganY)/2

        local currentPosA  = cc.p( self._touches[1].currentX , self._touches[1].currentY )
        local previousPosA = cc.p( self._touches[1].prevX , self._touches[1].prevY )
        local currentPosB  = cc.p( self._touches[2].currentX , self._touches[2].currentY )
        local previousPosB = cc.p( self._touches[2].prevX , self._touches[2].prevY )

        local currentVector = cc.pSub(currentPosA,currentPosB )
        local previousVector = cc.pSub(previousPosA,previousPosB)

        --scale
        local sizeDiff = cc.pGetLength(currentVector) / cc.pGetLength(previousVector)
        if display.height>640 then
            sizeDiff = sizeDiff>1 and 1+(sizeDiff-1)*1.4 or 1-(1-sizeDiff)*1.4 
        end
        local canScale = false
        if sizeDiff<1 then
            if self:getScaleX()>1 then
                self:scaleWorld(sizeDiff,middleX,middleY)
            end
        elseif sizeDiff>1 then
            if self:getScaleX()<3 then
                self:scaleWorld(sizeDiff,middleX,middleY)
            end
        end
    end
end

function BaseWorld:onWorldTouchEnded()
    if #self._touches==0 then return end
    local dis = cc.pGetDistance(cc.p(self._touches[1].currentX,self._touches[1].currentY),cc.p(self._touches[1].beganX,self._touches[1].beganY))
    if #self._touches == 1 and dis>6 then
        self._moveSpeed = 0.1
        self._endX = self._endX + (self._touches[1].currentX-self._touches[1].prevX)*6/self:getScaleX()
        self._endY = self._endY + (self._touches[1].currentY-self._touches[1].prevY)*6/self:getScaleY()
        self:modifyEndPosition()
    end

    if #self._touches==1 and dis<6 then
        local nx,ny = self:pixelPointToGrid(self._touches[1].currentX,self._touches[1].currentY)
        if self._gridData:checkInGrid( nx,ny ) then
            print(nx,ny , self._gridData:getNode(nx,ny).walkable )
        end
    end
end

function BaseWorld:scaleWorld( sizeDiff,middleX,middleY )
    self._scaleMatrix:indentity()
    self._scaleMatrix:scale(self:getScaleX(),self:getScaleY())
    self._scaleMatrix:translate(self:getPositionX(),self:getPositionY())
    self._scaleMatrix.tx = self._scaleMatrix.tx - middleX
    self._scaleMatrix.ty = self._scaleMatrix.ty - middleY

    self._scaleMatrix:scale(sizeDiff,sizeDiff)

    self._scaleMatrix.tx = self._scaleMatrix.tx + middleX
    self._scaleMatrix.ty = self._scaleMatrix.ty + middleY

    self:setScaleX(self._scaleMatrix.a)
    self:setScaleY(self._scaleMatrix.d)

    self:setPosition(self._scaleMatrix.tx,self._scaleMatrix.ty)

    self._endX = self._scaleMatrix.tx
    self._endY = self._scaleMatrix.ty


    if self._endY<display.height then 
        self._endY=display.height 
        self:setPositionY(display.height )
    elseif self._endY > MAP_HEIGHT*self:getScaleY() then
        self._endY = MAP_HEIGHT*self:getScaleY()
        self:setPositionY(self._endY)
    end

    if self._endX>0 then
        self._endX=0
        self:setPositionX(0)
    elseif self._endX<-MAP_WIDTH*self:getScaleX()+display.width then
        self._endX = -MAP_WIDTH*self:getScaleX()+display.width
        self:setPositionX(self._endX)
    end
end


--修正区域，防止地图滑出外面.
function BaseWorld:modifyEndPosition()
    if self._endY<display.height then self._endY=display.height 
    elseif self._endY > MAP_HEIGHT*self:getScaleY() then
        self._endY = MAP_HEIGHT*self:getScaleY()
    end

    if self._endX>0 then self._endX=0
    elseif self._endX<-MAP_WIDTH*self:getScaleX()+display.width then
        self._endX = -MAP_WIDTH*self:getScaleX()+display.width
    end
end


function BaseWorld:onClick( node )
    if node then
        -- print( "点击的网格坐标:",node:getNodeX(),node:getNodeZ() )
        node:stopAllActions()
        local action = cc.Sequence:create({ CCScaleTo:create(0.1,1.2),CCScaleTo:create(0.1,1) })
        node:runAction(action)
    end
end

return BaseWorld