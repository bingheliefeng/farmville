local Matrix = class()

function Matrix:ctor( a,b,c,d,tx,ty )
	self.a = a or 1 
	self.b = b or 0 
	self.c = c or 0 
	self.d = d or 1
	self.tx = tx or 0
	self.ty = ty or 0
end

function Matrix:rotate (angle) 
    local cos = math.cos(angle);
    local sin = math.sin(angle);

    local a1 = self.a;
    local c1 = self.c;
    local tx1 = self.tx;

    self.a = a1 * cos - self.b * sin;
    self.b = a1 * sin + self.b * cos;
    self.c = c1 * cos - self.d * sin;
    self.d = c1 * sin + self.d * cos;
    self.tx = tx1 * cos - self.ty * sin;
    self.ty = tx1 * sin + self.ty * cos;
    return self;
end

function Matrix:scale(x,y)
	self.a =self.a * x;
    self.d =self.d * y;
    self.tx = self.tx* x;
    self.ty = self.ty * y;
    return self;
end

function Matrix:translate( tx,ty)
	self.tx = self.tx+tx
	self.ty = self.ty + ty
	return self
end

function Matrix:indentity()
	self.a = 1 
	self.d = 1 
	self.b = 0
	self.c = 0
	self.tx = 0 
	self.ty = 0
	return self
end


return Matrix