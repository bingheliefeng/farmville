local isoUtil = require("ext.iso.isoUtil")

local IsoWorld = class("IsoWorld",function()
	return cc.Node:create()
end)

function IsoWorld:ctor( size , gridX , gridZ)

	self._size = size
	self._gridX = gridX
	self._gridZ = gridZ

	self._bg = nil 
	self._sceneLayer = cc.Node:create()
	self:addChild(self._sceneLayer,1)
	self._scenes = {}
end

function IsoWorld:getScenes()
	return self._scenes
end


function IsoWorld:addScene( isoScene )
	self._sceneLayer:addChild( isoScene)
	self._scenes[ #self._scenes+1 ] = isoScene 
	return isoScene
end

function IsoWorld:panSceneLayerTo( x , y )
	self._sceneLayer:setPosition( x,y)
end

function IsoWorld:getSceneOffsetX()
	return self._sceneLayer:getPositionX()
end
function IsoWorld:getSceneOffsetY()
	return self._sceneLayer:getPositionY()
end

--IsoWord坐标转成正确的网格坐标
function IsoWorld:pixelPointToGrid( px , py , offsetX, offsetY )
	offsetX = offsetX or 0
	offsetY = offsetY or 0

	local xx = (px-self:getPositionX())/self:getScaleX() - self:getSceneOffsetX() - offsetX  
	local yy = (py-self:getPositionY())/self:getScaleY() - self:getSceneOffsetY() - offsetY 
	return isoUtil.screenToIsoGrid( self._size,xx,yy)
end

--stage全局坐标转成IsoWorld场景像素坐标
function IsoWorld:globalPointToWorld( px,py )
	local xx = (px-self:getPositionX())/self:getScaleX() - self:getSceneOffsetX()
	local yy = (py-self:getPositionY())/self:getScaleY() - self:getSceneOffsetY()
	return xx,yy
end


function IsoWorld:setBg( bg )
	if self._bg ~= nil then
			self:removeChild( self._bg , true );
	end
	self._bg = bg
	self:addChild( self._bg , 0) 
end

function IsoWorld:clearWorld( clearUp )
	clearUp = clearUp or false

	for _,scene in ipairs(self._scenes) do
		scene:clearScene(cleanUp)
	end
	self._scene = {}
end


return IsoWorld