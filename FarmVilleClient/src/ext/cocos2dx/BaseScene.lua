local BaseScene = class("BaseScene",function()
	return cc.Scene:create()
end)

function BaseScene:ctor()
	self:registerScriptHandler(function(tag)
	   if tag=="enter" then
           if self.onEnter then  self:onEnter() end
	   elseif tag=="exit" then
           if self.onEixt then  self:onEixt() end
	   elseif tag =="enterTransitionFinish" then
	       if self.enterTransitionFinish then self:enterTransitionFinish() end
	   end
	end)
end


--[[

每帧执行.
--]]
function BaseScene:enableUpdate( flag , updateFunc )
    if flag then
        self:unscheduleUpdate()
        self:scheduleUpdateWithPriorityLua(updateFunc,0)
    else
        self:unscheduleUpdate()
    end
end



-------------------------------------------------------
----设置定时器
----@function [parent=#ui.BaseScene] setInterval
----@param #number delay 间隔时间
----@param #function func 结束时的回调函数
----@return #number 返回当前定时器id
-------------------------------------------------------
function BaseScene:setTimeout( delay , func , ...)
    if self._timeId then
        self:clearTimeoutId(self._timeoutId)
    end
    self._timeoutId = self:getScheduler():scheduleScriptFunc(function()
        if arg then
            func(unpack(arg))
        else
            func()
        end
        self:clearTimeout(self._timeoutId)
    end,delay,false) 
    return self._timeoutId
end


-------------------------------------------------------
----清除定时器
----@function [parent=#ui.BaseScene] clearTimeout
----@param #number timeoutId 定时器id
-------------------------------------------------------
function BaseScene:clearTimeout( timeoutId )
    self:getScheduler():unscheduleScriptEntry(timeoutId)
end


-------------------------------------------------------
----设置定时器
----@function [parent=#ui.BaseScene] setInterval
----@param #number interval 间隔时间
----@param #function func 结束时的回调函数
----@return #number 返回当前定时器id
-------------------------------------------------------
function BaseScene:setInterval( interval,func,...)
    if self._intevalId then
        self:clearInterval(self._intevalId)
    end
    self._intevalId = self:getScheduler():scheduleScriptFunc(function()
        if arg then
            func(unpack(arg))
        else
            func()
        end
        self:clearInterval(self._intevalId)
    end,interval,false) 
    return self._intevalId
end


-------------------------------------------------------
----清除定时器
----@function [parent=#ui.BaseScene] clearInterval
----@param #number intervalId 定时器id
-------------------------------------------------------
function BaseScene:clearInterval( intervalId )
    self:getScheduler():unscheduleScriptEntry(intervalId)
end




return BaseScene