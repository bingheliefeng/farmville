require("Config")
require("ext.extInit")

App = App or {}

local function enterScene(scene)
    if cc.Director:getInstance():getRunningScene() then
        cc.Director:getInstance():replaceScene(scene)
    else
        cc.Director:getInstance():runWithScene(scene)
    end
end



function App.run()
    --第一个进入的场景
    App.enterUITestScene()
end


function App.enterLoadingScene()
    enterScene( require("game.scene.LoadingScene").new() )
end

function App.enterGameScene()
    enterScene( require("game.scene.GameScene").new() )
end

function App.enterUITestScene()
    enterScene(require("game.scene.UITestScene").new() )
end

return App