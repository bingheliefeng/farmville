--[[
function UITestScene:createCharFont(container)
    local font = CharFont.new("fonts/labelatlas.png",16,32,".")
    font.touchEnable = true
    font:setText("123459.123")
    font:setUIPosition(300,300)
    font:setSize(100,50) --自动缩放
    font:showBorder()
    container:addChild(font)
    font.dispatcher:addEventListener(TouchEvent.CLICK,self.onCharFontClick,self)
end
function UITestScene:onCharFontClick(evt)
    print("click Char Font")
end
--]]

----@module ext.ui.CharFont 等宽高字符
local CharFont = class("CharFont",require("ext.ui.Component"))

----------------------------------------------------------
----@function [parent=#ext.ui.CharFont] 等宽的字符字体
----@param #string  text 显示的文本
----@param #string fontFilePath 字体图片
----@param #int charWidth 字符宽
----@param #int charHeight 字符高
----@param #string firstChar 第一个字符
------------------------------------------------------------
function CharFont:ctor(text, fontFilePath,charWidth,charHeight,firstChar,width,height)
    CharFont.super.ctor(self)
	
    self._fontFilePath = assert(fontFilePath,"fontFilePath 不能为nil")
    self._text = text or ""
    self._fontSize = self._fontSize or 24
    self._color = cc.c3b(255,255,255)
    self._hAlign = cc.TEXT_ALIGNMENT_CENTER
    self._vAlign = cc.VERTICAL_TEXT_ALIGNMENT_CENTER
    
    width = width or 0
    height = height or 0
    self:setContentSize(width,height)

    local label = cc.Label:createWithCharMap(fontFilePath,charWidth,charHeight,string.byte(firstChar))
    assert(label,"label为空")

    if width >0 then
        label:setWidth(width)
    end
    if height >0 then
        label:setHeight(height)
    end
    self._label = label
    self._label:setString(self._text)
    self:addChild( self._label )

end


function CharFont:getLabel()
    return self._label
end

--------------------------------------------------------------
----设置显示的文本
----@function [parent=#ext.ui.CharFont] setText
----@param #string str 字符串
----------------------------------------------------------------
function CharFont:setText(str)
    self._label:setString(str)
    if self:getContentSize().width>0 and self:getContentSize().height>0 then
        self:setSize(self:getContentSize().width,self:getContentSize().height)
    end 
    return self
end

--------------------------------------------------------------
----设置文本颜色
----@function [parent=#ext.ui.CharFont] setColor
----@param #cc.c3b ccColor3 颜色
--------------------------------------------------------------
function CharFont:setColor(ccColor3)
    self._label:setColor(ccColor3)
    return self
end

--------------------------------------------------------------
----设置水平对齐方式
----@function [parent=#ext.ui.CharFont] setHAlign
----@param #number ccTextAlign 水平对齐方式
----------------------------------------------------------------
function CharFont:setHAlign(ccTextAlign)
    if self._hAlign~=ccTextAlign then
        self._hAlign = ccTextAlign
        self._label:setHorizontalAlignment(self._hAlign)
    end
    return self
end

--------------------------------------------------------------
----设置竖起对齐方式
----@function [parent=#ext.ui.CharFont] setVAlign
----@param #number ccVerticalTextAlign 竖直对齐方式
----------------------------------------------------------------
function CharFont:setVAlign(ccVerticleTextAlign)
    if self._vAlign~=ccVerticleTextAlign then
        self._vAlign = ccVerticleTextAlign
        self._label:setVerticalAlignment(self._vAlign)
    end
    return self
end

--------------------------------------------------------------
----设置文本的锚点
----@function [parent=#ext.ui.CharFont] setAnchorPoint
----@param #number h 水平锚点 0-1
----@param #number v 竖直锚点 0-1
----------------------------------------------------------------
function CharFont:setAnchorPoint(h,v)
    self._label:setAnchorPoint(h,v)
end



--------------------------------------------------------------
----设置文本的宽
----@function [parent=#ext.ui.CharFont] setTextWidth
----@param #number width 宽度
--------------------------------------------------------------
function CharFont:setTextWidth(width)
    self._label:setWidth(width)
    return self
end


--------------------------------------------------------------
----设置文本的高
----@function [parent=#ext.ui.CharFont] setTextHeight
----@param #number height 高度
----------------------------------------------------------------
function CharFont:setTextHeight(height)
    self._label:setHeight(height)
    return self
end


----------------------------------------------------------------
----重写setContentSize , 如果文本大于此宽高时，会自动缩放
----@function [parent=#ext.ui.CharFont] setContentSize
----@param #number width 宽度
----@param #number height 高度
----------------------------------------------------------------
function CharFont:setContentSize(width,height)
    CharFont.super.setContentSize(self,width,height)
    if self._label then
        local labelSize = self._label:getContentSize()
        if labelSize.width>width or labelSize.height>height then
            local scaleW = labelSize.width/width
            local scaleH = labelSize.height/height
            local scale = 1
            if scaleW>scaleH then
                scale = 1/scaleW         
            else
                scale = 1/scaleH 
            end
            self._label:setScale(scale,scale)
        end
    end
    return self
end

return CharFont