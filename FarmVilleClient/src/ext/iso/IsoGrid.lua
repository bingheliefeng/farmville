local isoUtil = require("ext.iso.isoUtil")

local IsoGrid = class("IsoGrid",function()
	return cc.Node:create()
end)

function IsoGrid:ctor(  size , gridX , gridZ , alpha )
	self._gridX = gridX
	self._gridZ = gridZ
	self._size = size
	self._alpha = alpha

	self:render()
end

function IsoGrid:render()
    self:removeAllChildren(true)

	local drawNode = cc.DrawNode:create()
	local lineColor = cc.c4f(1,0,0,self._alpha)
	local thick = 2

	local  p = {x=0,y=0,z=0}
	for i=0,self._gridX do
		p.x = i*self._size
		p.z = 0 
		local startX,startY = isoUtil.isoToScreen( p.x,p.y,p.z)
		p.z = self._gridZ*self._size
		local endX,endY = isoUtil.isoToScreen( p.x,p.y,p.z)
        drawNode:drawSegment(cc.p(startX,startY), cc.p(endX,endY),thick, lineColor)
	end


	for i=0,self._gridZ do
		p.z = i*self._size
		p.x = 0 
		local startX,startY = isoUtil.isoToScreen( p.x,p.y,p.z)
		p.x = self._gridX*self._size
		local endX,endY = isoUtil.isoToScreen( p.x,p.y,p.z)
        drawNode:drawSegment(cc.p(startX,startY), cc.p(endX,endY),thick, lineColor)
	end

	self:addChild(drawNode)

end

return IsoGrid 