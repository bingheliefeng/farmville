local GameWorld = require("game.core.GameWorld")

local GameScene = class("GameScene",BaseScene)
function GameScene:ctor()
    GameScene.super.ctor(self)
end


function GameScene:onEnter()
    self.gameWorld = GameWorld.new()
    self:addChild(self.gameWorld)
    
    self:enableUpdate(true,handler(self,self.update) )
end

function GameScene:update(dt)
    self.gameWorld:update(dt)
end

function GameScene:onExit()

end


return GameScene
