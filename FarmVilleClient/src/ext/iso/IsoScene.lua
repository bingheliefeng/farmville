local IsoScene = class("IsoScene",IsoObject)

function IsoScene:ctor( size, spanX , spanZ  )
	spanX = spanX or 1
	spanZ = spanZ or 1
    
    IsoScene.super.ctor(self,size,spanX,spanZ)

	self._sprites = {} --所有的sprite
	self._gridData = nil
end


function IsoScene:createGridData( gridX, gridZ )
	self._gridData = PathGrid.new( gridX , gridZ )
end

function IsoScene:getGridData()
	return self._gridData
end

--设置网格数据，有时需要共用一个网格数据源
function IsoScene:setGridData(pathGrid)
	this._gridData = gird
end


function IsoScene:addIsoObject( object , isSort )
	isSort = isSort or true

	self:addChild(object,0)
	self._sprites[#self._sprites+1] = object 
	if isSort then
		self:sortIsoObject(object);
	end
	return object
end

function IsoScene:removeIsoObject( obj )
	self:removeChild(obj)
	for i,v in ipairs(self._sprites) do
		if v == obj then 
			table.remove(self._sprites,i)
		end
	end
	return obj
end

--适应于都是规则的物体
function IsoScene:sortIsoObject( obj )
	self:reorderChild(obj,math.floor(obj:getDepth()))
end

function IsoScene:sortAll()
	for _,v in ipairs(self._sprites) do
		self:sortIsoObject(v)
		v.isSort = false 
	end
end

function IsoScene:getIsoObjectByNodePos( nodeX,nodeZ )
	for _,v in ipairs(self._sprites) do
		if v:getNodeX()==nodeX and v:getNodeZ()==nodeZ then
			return v
		end
	end
	return nil
end


function IsoScene:getIsoChildren()
	return self._sprites
end


function IsoScene:clearScene( clearUp )
	clearUp = clearUp or false
	self:removeAllChildren(clearUp)
	for _,v in ipairs(self._sprites) do
		v:dispose()
	end
	self._sprites={}
end




return IsoScene