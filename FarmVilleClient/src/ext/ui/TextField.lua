--[[
function UITestScene:createTextField(container)
local font = "fonts/font-issue1343.fnt" --"fonts/Marker Felt.ttf"
    local tf = TextField.new("begin hello world , hello world , hello world hello world , hello world , hello world, hello world , hello world end",font)
    tf:setHAlign(cc.TEXT_ALIGNMENT_RIGHT)
    tf:setVAlign(cc.VERTICAL_TEXT_ALIGNMENT_TOP)
    tf:setTextWidth(200)
    --    tf:setTextHeight(100)
    tf:setColor(cc.c3b(255,255,0))
    tf:setContentSize(560,100)
    tf.touchEnable = true
    tf:setUIPosition(display.width/2,display.height/2)
    --    tf:setAnchorPoint(1,1)
    tf:showBorder()
    container:addChild(tf)
    tf.dispatcher:addEventListener(TouchEvent.CLICK,self.onTFClick , self)
end
function UITestScene:onTFClick(evt)
    print("TextField Click")
end
--]]

----@module ext.ui.TextField  文本
local TextField = class("TextField",require("ext.ui.Component"))

--------------------------------------------------------------
----构造函数
----@function [parent=#ext.ui.TextField] ctor
----@param #string text="" 要显示的字符串
----@param #string fontFilePath=Arial 字符的路径或名称
----@param #number fontSize=24 字体大小
----@param #cc.c3b ccColor3=cc.c3b(255,255,255) 字体颜色
----@param #number hAlign=cc.TEXT_ALIGNMENT_CENTER   cc.Text_ 常量
----@param #number vAlign=cc.VERTICAL_TEXT_ALIGNMENT_CENTER   cc.VERTICAL_Text_ 常量
----@param #number width=0 宽
----@param #number height=0 高
--------------------------------------------------------------
function TextField:ctor( text,fontFilePath,fontSize,ccColor3,hAlign,vAlign,width,height )
	TextField.super.ctor(self)
	
    self._fontFilePath = fontFilePath or "Arial"
	self._text = text or ""
    self._fontSize = self._fontSize or 24
    self._color = ccColor3 or cc.c3b(255,255,255)
    self._hAlign = hAlign or cc.TEXT_ALIGNMENT_CENTER
    self._vAlign = vAlign or cc.VERTICAL_TEXT_ALIGNMENT_CENTER
	width = width or 0
	height = height or 0
	self:setContentSize(width,height)
	
	local label
    local fnt=string.find(string.lower(self._fontFilePath),".fnt")
    local ttf=string.find(string.lower(self._fontFilePath),".ttf")
    if fnt~=nil then
        label = cc.Label:createWithBMFont(self._fontFilePath,self._text)
    elseif ttf~=nil then
        self._config = {fontFilePath=self._fontFilePath,fontSize=self._fontSize}
        label = cc.Label:createWithTTF (self._config,self._text)
    else
        --系统字体
        label = cc.Label:createWithSystemFont(self._fontFilePath,self._text,float,size,texthalignment,textvalignment)
    end
    
    if width >0 then
        label:setWidth(width)
    end
    if height >0 then
        label:setHeight(height)
    end
    label:setHorizontalAlignment(self._hAlign)
    label:setVerticalAlignment(self._vAlign)

    self._label = label 
	self:addChild( self._label )
end


--------------------------------------------------------------
----获取cocos2dx 中的cc.Label
----@function [parent=#ext.ui.TextField] getLabel
----@return #cc.Label 返回Label
--------------------------------------------------------------
function TextField:getLabel()
	return self._label
end



--------------------------------------------------------------
----设置显示的文本
----@function [parent=#ext.ui.TextField] setText
----@param #string str 字符串
----------------------------------------------------------------
function TextField:setText(str)
    self._label:setString(str)
    if self:getContentSize().width>0 and self:getContentSize().height>0 then
        self:setContentSize(self:getContentSize().width,self:getContentSize().height)
    end 
    return self
end

--------------------------------------------------------------
----设置文本颜色
----@function [parent=#ext.ui.TextField] setColor
----@param #cc.c3b ccColor3 颜色
--------------------------------------------------------------
function TextField:setColor(ccColor3)
    self._label:setColor(ccColor3)
    return self
end

--------------------------------------------------------------
----设置水平对齐方式
----@function [parent=#ext.ui.TextField] setHAlign
----@param #number ccTextAlign 水平对齐方式
----------------------------------------------------------------
function TextField:setHAlign(ccTextAlign)
    if self._hAlign~=ccTextAlign then
        self._hAlign = ccTextAlign
        self._label:setHorizontalAlignment(self._hAlign)
    end
    return self
end

--------------------------------------------------------------
----设置竖起对齐方式
----@function [parent=#ext.ui.TextField] setVAlign
----@param #number ccVerticalTextAlign 竖直对齐方式
----------------------------------------------------------------
function TextField:setVAlign(ccVerticleTextAlign)
    if self._vAlign~=ccVerticleTextAlign then
        self._vAlign = ccVerticleTextAlign
        self._label:setVerticalAlignment(self._vAlign)
    end
    return self
end

--------------------------------------------------------------
----设置文本的锚点
----@function [parent=#ext.ui.TextField] setAnchorPoint
----@param #number h 水平锚点 0-1
----@param #number v 竖直锚点 0-1
----------------------------------------------------------------
function TextField:setAnchorPoint(h,v)
	self._label:setAnchorPoint(h,v)
end

--------------------------------------------------------------
----设置文本的宽
----@function [parent=#ext.ui.TextField] setTextWidth
----@param #number width 宽度
--------------------------------------------------------------
function TextField:setTextWidth(width)
    self._label:setWidth(width)
    return self
end

--------------------------------------------------------------
----设置文本的高
----@function [parent=#ext.ui.TextField] setTextHeight
----@param #number height 高度
----------------------------------------------------------------
function TextField:setTextHeight(height)
    self._label:setHeight(height)
    return self
end


----------------------------------------------------------------
----重写setContentSize , 如果文本大于此宽高时，会自动缩放
----@function [parent=#ext.ui.TextField] setContentSize
----@param #number width 宽度
----@param #number height 高度
----------------------------------------------------------------
function TextField:setContentSize(width,height)
    TextField.super.setContentSize(self,width,height)
    if self._label then
        local labelSize = self._label:getContentSize()
        if labelSize.width>width or labelSize.height>height then
            local scaleW = labelSize.width/width
            local scaleH = labelSize.height/height
            local scale = 1
            if scaleW>scaleH then
                scale = 1/scaleW         
            else
                scale = 1/scaleH 
            end
            self._label:setScale(scale,scale)
        end
    end
    return self
end

return TextField