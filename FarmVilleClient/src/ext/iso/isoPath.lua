--[[

用于寻路，和iso地图数据结构


--]]


PathLink = class("PathLink")

function PathLink:ctor( pathNode , cost )
	self.node = node
	self.cost = cost
end




PathNode = class("PathNode")

function PathNode:ctor(nodeX,nodeY)
		self.x = nodeX
		self.y = nodeY 
		self.f = 0 
		self.g = 0 
		self.h = 0
		self.walkable = false
		self.parent = nil 
		self.links= nil
		self.version = 1
end


function PathNode:clone()
	-- body
	local node = PathNode.new(self.x,self.y)
	return node 
end







PathGrid = class("PathGrid")

function PathGrid:ctor( gridX , gridZ )
	self._gridX = gridX 
	self._gridZ = gridZ

	self._startNode = nil 
	self._endNode = nil 
	self._nodes = {}

	self._type = 0
		
	self._straightCost = 1.0
	self._diagCost = math.sqrt(2)

	for i=1,gridX do
		self._nodes[i] = {}
		for j=1,gridZ do
			self._nodes[i][j] = PathNode.new(i,j)
		end
	end
end

function PathGrid:setAllWalkable( value )
	for i=1,self._gridX do
		for j=1,self._gridZ do
			self._nodes[i][j].walkable = value 
		end
	end
end

function PathGrid:changeSize( gridX , gridZ )
	local nodes = {}
	for i=1,gridX do
		nodes = {}
		for j=1,gridZ do
			if i<=gridX and j<=gridZ then
				nodes[i][j] = self._nodes[i][j]
			else
				nodes[i][j] = PathNode.new(i,j)
			end
		end
	end
	self._gridX = gridX 
	self._gridZ = gridZ
	self._nodes = nodes 
end

function PathGrid:getNode(nodeX,nodeZ)
	return self._nodes[nodeX][nodeZ]
end

function PathGrid:setStartNode( nodeX,nodeZ )
	self._startNode = self._nodes[nodeX][nodeZ] 
end

function PathGrid:setEndNode(nodeX,nodeZ)
	self._endNode = self._nodes[nodeX][nodeZ]
end

function PathGrid:getNodesByWalkable( walkable )
	local nodes = {}
	for i=1,self._gridX do
		for j=1,self._gridZ do
			local node = self._nodes[i][j]
			if node.walkable == walkable then
				nodes[#nodes+1] = node 
			end
		end
	end
	return nodes
end

--node PathNode
--type	0八方向 1四方向 2跳棋
function PathGrid:initNodeLink(node,type)
	local startX = math.max(1 , node.x)
	local endX = math.min(self._gridZ, node.x)
	local startY = math.max(1, node.y)
	local endY = math.min( self._gridX , node.y )
	node.links = {}
	for i=startX,self._gridX do
		for j=startY,self._gridZ do
			repeat
				local test = self:getNode(i,j)
				if test == node or not test.walkable then break end
				if type~=2 and i~=node.x and j~=node.y then
					local test2 = self:getNode(node.x,j)
					if not test2.walkable then break end
					test2 = self:getNode(i,node.y)
					if not test2.walkable then break end
				end
				local cost = self._straighCost
				if not (node.x==test.x or node.y==test.y ) then
					if type==1 then break end
					if type==2 and (node.x-test.x)*(node.y-test.y)==1 then break end
					if type==2 then cost = self._straightCost 
					else cost = self._diagCost end
				end
				node.links[#node.links+1] = PathNode.new(test, cost )
				break
			until true
		end
	end

end

function PathGrid:checkInGrid( nodeX,nodeZ )
	if  nodeX<1 or nodeZ<1 or nodeX>self._gridX or nodeZ>self._gridZ then
		return false
	end
	return true
end

function PathGrid:setWalkable(nodeX,nodeZ,value)
	self._nodes[nodeX][nodeZ].walkable = value
end

--type	0四方向 1八方向 2跳棋
function PathGrid:calculateLinks( type )
	type = type or 0
	self._type = type
	for i=1,self._gridX do
		for j=1,self._gridZ do
			self:initNodeLink(_nodes[i][j], type)
		end
	end
end


function PathGrid:getType()
	return self._type
end

function PathGrid:getStartNode()
	return self._startNode
end

function PathGrid:getEndNode()
	return self._endNode
end

function PathGrid:getGridX()
	return self._gridX
end

function PathGrid:getGridZ()
	return self._gridZ
end

function PathGrid:clone()
	local grid = PathGrid.new(self._gridX,self._gridZ)
	for i=1,self._gridX do
		for j=1,self._gridZ do
			gird:getNode(i,j).walkable = self:getNode(i,j).walkable 
		end
	end
	return grid
end









BinaryHeap = class("BinaryHeap")
function BinaryHeap:ctor( justMinFun )
	if justMinFun then
		self._justMinFun = justMinFun 
	else
		self._justMinFun = function(x,y)
			return x < y
		end
	end
	self.a={-1}
end

function BinaryHeap:ins( value )
	local p = #a --a的长度
	a[p+1] = value
	local pp = p *2

	while p > 1 and self:_justMinFun(a[p], a[pp]) do
		a[p],a[pp] = a[pp],a[p] --交换
		pp = p *2
	end
end

function BinaryHeap:pop()
	local min = a[1]
	a[1] = a[#a]
	table.remove( a,#a )

	local p = 1
	local l = #a
	local sp1 = p /2 
	local sp2 = sp1 + 1
	while sp1 <= l do
		if sp2 <= l then
			minp = self._justMinFun(a[sp2], a[sp1]) or sp2 and sp1
		else
			minp = sp1
		end
		if self._justMinFun(a[minp], a[p]) then
			a[p],a[minp] = a[minp],a[p]
			sp1 = p /2
			sp2 = sp1 + 1
		else
			break
		end
	end
	return min
end










Astar = class("Astar")

function Astar:ctor( pathGrid )
	self._grid = pathGrid 
	self._open = nil --BinaryHeap
	self._endNode = nil 
	self._startNode = nil 
	self._path = nil --数组
	self._straightCost = 1.0
	self._diagCost = math.sqrt(2)

	self.nowversion = 1
	self.twoOneTwoZero = 2 * math.cos(math.pi / 3)
	self.heuristic = self.euclidian2
end

function Astar:justMin(pathNode1, pathNode2)
	return pathNode1.f < pathNode2.f
end

function Astar:findPath()
	self._endNode = self._grid.endNode
	self.nowversion = self.nowversion+1
	self._startNode = self._grid.startNode
	self._open = BinaryHeap.new(self.justMin)
	self._startNode.g = 0 
	return self:search()
end

function Astar:search()
	local node = self._startNode
	node.version = self.nowversion
	while node ~= self._endNode do
		local len = #node.links
		for i=1,len do
			local test = node.links[i].node
			local cost = node.links[i].cost
			local g = node.g + cost
			local h = self.heuristic(test)
			local f = g + h
			if test.version == self.nowversion then
				if test.f > f then
					test.f = f
					test.g = g
					test.h = h
					test.parent = node
				end
			else
				test.f = f
				test.g = g
				test.h = h
				test.parent = node
				self._open.ins(test)
				test.version = nowversion;
			end
			
		end
		if #self._open.a == 1 then
			return false
		end
		node = table.remove(self._open,#self._open) --移除最后一个元素
	end
	buildPath();
	return true;
end


function Astar:buildPath()
	local node = self._endNode
	self._path = {node}
	while node ~= self._startNode do
		node = node.parent
		table.insert(self._path,1,node)
	end
end

function Astar:getPath()
	return self._path
end

function Astar:manhattan(pathNode)
	return math.abs(pathNode.x - self._endNode.x) + math.abs(pathNode.y - self._endNode.y)
end

function Astar:manhattan2(pathNode)
	local dx = math.abs(node.x - self._endNode.x)
	local dy = math.abs(node.y - self._endNode.y)
	return dx + dy + math.abs(dx - dy) / 1000
end

function Astar:euclidian(pathNode)
	local dx = pathNode.x - self._endNode.x
	local dy = pathNode.y - self._endNode.y
	return math.sqrt(dx * dx + dy * dy)
end

function Astar:euclidian2(pathNode)
	local dx = pathNode.x - self._endNode.x
	local dy = pathNode.y - self._endNode.y
	return dx * dx + dy * dy
end

function Astar:diagonal( pathNode )
	local dx = Math.abs( pathNode.x - self._endNode.x )
	local dy = Math.abs( pathNode.y - self._endNode.y )
	local diag = math.min(dx, dy)
	local straight = dx + dy
	return self._diagCost * diag + self._straightCost * (straight - 2 * diag)
end

function Astar:chineseCheckersEuclidian2( pathNode)
	local y = pathNode.y / self.twoOneTwoZero
	local x = pathNode.x + pathNode.y / 2
	local dx = x - self._endNode.x - self._endNode.y / 2
	local dy = y - self._endNode.y / self.twoOneTwoZero
	return math.sqrt(dx * dx + dy * dy)
end
