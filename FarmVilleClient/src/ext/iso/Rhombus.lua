local Rhombus  = class("Rhombus",function()
	return cc.Node:create()
end)


function Rhombus:ctor( size , color )
	color = color  or  cc.c4b(255,255,255,255)
	self:draw(color , size )
end


function Rhombus:draw(size , color )
	self:removeAllChildren(true)
	-- body
	local points = {}
	points[1] = {x=0,y=0}
	points[2] = {x=size,y=size/2}
	points[3] = {x=0,y=size}
	points[4] = {x=-size,y=size/2}
	self.node = cc.DrawNode:create()
    self.node:drawPolygon ( points, #points,cc.c4b(0,0,0,0),borderWidth,color)
	self:addChild(node)
end


return Rhombus